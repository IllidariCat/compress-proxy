package main

import (
	"bufio"
	"bytes"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const (
	HOST                 = "localhost"
	TcpPort              = "8080"
	HttpPort             = "8083"
	SCHEME               = "tcp"
	CLRF                 = "\r\n"
	HeaderValueSeparator = ": "
	HeaderContentLength  = "content-length"
	HeaderContentType    = "content-type"
	HeaderContentRange   = "content-range"
	HeaderHost           = "host"
	HTTPPort             = "80"
)

const (
	JPEG	= "image/jpeg"
	PNG		= "image/png"
)

var JPEGQualityDefault = jpeg.Options {
	Quality: 1,
}

type HTTPMessage struct {
	Http string
	Headers map[string][]string
	Body []byte
}

func (m HTTPMessage) Bytes() []byte {
	headers := make([]string, 0)
	for header, values := range m.Headers {
		for _, value := range values {
			headers = append(headers, header+HeaderValueSeparator+value+CLRF)
		}
	}
	data := make([]byte, 0)
	data = append(data, []byte(m.Http)...)
	for _, header := range headers {
		data = append(data, []byte(header)...)
	}
	data = append(data, []byte(CLRF)...)
	if m.Body != nil {
		data = append(data, m.Body...)
	}
	return data
}

func (m HTTPMessage) ToString(body bool) string {
	headers := make([]string, 0)
	for header, values := range m.Headers {
		for _, value := range values {
			headers = append(headers, header+HeaderValueSeparator+value+CLRF)
		}
	}
	data := make([]string, 0)
	data = append(data, m.Http)
	data = append(data, headers...)
	data = append(data, CLRF)
	if m.Body != nil && body {
		data = append(data, string(m.Body))
	}
	return strings.Join(data, "")
}

func (m HTTPMessage) String() string {
	return m.ToString(true)
}

func (m *HTTPMessage) ParseHeaders(raw []string) {
	for _, header := range raw {
		split := strings.SplitN(header, HeaderValueSeparator, 2)
		key := strings.ToLower(split[0])
		if _, exist := m.Headers[key]; !exist {
			m.Headers[key] = make([]string, 0)
		}
		if len(split) == 2 {
			m.Headers[key] = append(m.Headers[key], strings.TrimSuffix(split[1], CLRF))
		}
	}
}

func main () {
	//HTTP
	proxy := &Proxy{}
	go func() {
		if err := http.ListenAndServe(":"+HttpPort, proxy); err != nil {
			log.Printf("[Fatal] Failed to create http proxy-server: %s", err)
			os.Exit(1)
		}
	}()
	//TCP
	server, err := net.Listen(SCHEME, HOST+":"+TcpPort)
	if err != nil {
		log.Printf("[Fatal] Failed to create proxy-server: %s", err)
		os.Exit(1)
	}
	defer func () {
		err := server.Close()
		if err != nil {
			log.Printf("[Error] Failed to stop tcp proxy-server: %s", err)
		}
	}()
	for {
		connection, err := server.Accept()
		if err != nil {
			log.Printf("[Error] Failed to accept incoming connection: %s", err)
		}
		log.Printf("[Debug] Accepted connetion from remote addres %s", connection.RemoteAddr())
		go handle(connection)
	}
}

func compress(image []byte, content string) []byte {
	split := strings.Split(content, ";")
	actualType := split[0]
	buffer := bytes.NewBuffer(image)
	switch actualType {
		case JPEG:
			img, err := jpeg.Decode(buffer)
			if err != nil {
				log.Printf("[Error] Failed to decode JPEG image, but content-type points to it: %s", err)
				return image
			}
			var data bytes.Buffer
			err = jpeg.Encode(&data, img, &JPEGQualityDefault)
			if err != nil {
				log.Printf("[Error] Failed to encode compressed JPEG image, even though it was decoded: %s", err)
				return image
			}
			return data.Bytes()
		case PNG:
			img, err := png.Decode(buffer)
			if err != nil {
				log.Printf("[Error] Failed to decode PNG image, but content-type points to it: %s", err)
				return image
			}
			encoder := png.Encoder{
				CompressionLevel: png.BestCompression,
			}
			var data bytes.Buffer
			err = encoder.Encode(&data, img)
			if err != nil {
				log.Printf("[Error] Failed to encode compressed PNG image, even though it was decoded: %s", err)
				return image
			}
			break
		default:
			return image
	}
	return image
}

func parseHTTP(connection net.Conn) (*HTTPMessage, error) {
	wrapper := bufio.NewReader(connection)
	message := HTTPMessage{
		Http:    "",
		Headers: make(map[string][]string),
		Body:    nil,
	}
	lines := make([]string, 0)
	for {
		if data, err := wrapper.ReadBytes('\n'); err != nil {
			if err != io.EOF {
				log.Printf("[Error] Failed to read message: %s", err)
			}
			return nil, err
		} else {
			line := string(data)
			if line == CLRF {
				message.Http = lines[0]
				message.ParseHeaders(lines[1:])
				if arr, exists := message.Headers[HeaderContentLength]; exists {
					if len(arr) == 0 {
						log.Printf("[Warning] Content-Length HTTP header was sent without value, if bode present it won't be processed")
					} else if len(arr) > 1 {
						log.Printf("[Warning] Content-Length HTTP header has multiple valeus, body won't be processed")
					} else {
						length, err := strconv.Atoi(strings.TrimSuffix(arr[0], CLRF))
						if err != nil {
							log.Printf("[Warning] Content-Length HTTP header has non-integer value, body won't be processed")
						} else {
							message.Body = make([]byte, length)
							if n, err := io.ReadFull(wrapper, message.Body); err != nil {
									log.Printf("[Error] Failed to read body: %s", err)
									return nil, err
							} else {
								log.Printf("[Debug] Read %d bytes of data. Content-Length: %d", n, length)
							}
						}
					}
				}
				return &message, nil
			} else {
				lines = append(lines, line)
			}
		}
	}
}

func closeConnection(connection net.Conn) {
	if err := connection.Close(); err != nil {
		log.Printf("[Error] Failed to closeConnection remote connection: %s", err)
	}
}

func handle(connection net.Conn) {
	message, err := parseHTTP(connection)
	if err != nil {
		closeConnection(connection)
	} else {
		log.Print(message.ToString(false))
		if arr, exists := message.Headers[HeaderHost]; exists && len(arr) == 1 {
			host := arr[0]
			var forward net.Conn
			var forwardError error
			if strings.Contains(host, ":") {
				forward, forwardError = net.Dial("tcp", host)
			} else {
				forward, forwardError = net.Dial("tcp", host+":"+HTTPPort)
			}
			if forwardError != nil {
				log.Printf("[Error] Failed to connect to remote host %s with http schema", host)
				closeConnection(connection)
			} else {
				if _, err := forward.Write(message.Bytes()); err != nil {
					log.Printf("[Error] Failed to write to remote remote host: %s", err)
					closeConnection(forward)
					closeConnection(connection)
				}
				response, err := parseHTTP(forward)
				closeConnection(forward) //Получили ответ или нет, запрос мы сделали, больше соединение с хостом нам не нужено
				if err != nil {
					closeConnection(connection)
				} else {
					log.Print(response.ToString(false))
					if arr, exists := response.Headers[HeaderContentType]; exists && len(arr) == 1 {
						if _, exists := response.Headers[HeaderContentRange]; exists {
							log.Printf("[Warning] Partial Content! Cannot compress image with none-full data")
						} else {
							beforeCompressionLength := len(response.Body)
							response.Body = compress(response.Body, arr[0])
							afterCompressionLength := len(response.Body)
							if afterCompressionLength != beforeCompressionLength {
								log.Printf("[Debug] Bytes: %d -> %d, ratio: %f", beforeCompressionLength, afterCompressionLength,
									float32(beforeCompressionLength) / float32(afterCompressionLength))
								response.Headers[HeaderContentLength][0] = strconv.Itoa(afterCompressionLength)
							}
						}
					}
					if _, err := connection.Write(response.Bytes()); err != nil {
						log.Printf("[Error] Failed to write to remote remote host: %s", err)
					}
					closeConnection(connection) //Отвправили ответ, даже если с ошибкой, все равно закрываем соединение
				}
			}
		} else {
			log.Printf("[Error] Host header not present or contains multiple values: %+v", arr)
			closeConnection(connection)
		}
	}
}

type Proxy struct {}

func (p *Proxy) ServeHTTP(wr http.ResponseWriter, r *http.Request) {
	client := http.DefaultClient
	var buf bytes.Buffer
	if _, err := io.Copy(wr, r.Body); err != nil {
		log.Printf("[Error] Failed to read body: %s", err)
		return
	}
	req, err := http.NewRequest(r.Method, r.URL.String(), &buf)
	if err != nil {
		log.Printf("[Error] Failed to create request to remote server: %s", err)
		return
	}
	req.Header = r.Header.Clone()
	response, err := client.Do(req)
	if err != nil {
		log.Printf("[Error] Failed to write to remote remote host: %s", err)
		return
	}
	for header, values := range response.Header {
		if strings.ToLower(header) == HeaderContentLength {
			continue
		}
		for _, value := range values {
			wr.Header().Set(header, value)
		}
	}
	if _, err = io.Copy(&buf, response.Body); err != nil {
		log.Printf("[Error] Failed to read resposne body: %s", err)
		return
	}
	data := buf.Bytes()
	var content string
	partial := false
	for header, values := range response.Header {
		lower := strings.ToLower(header)
		if lower == HeaderContentType {
			content = values[0]
		} else if lower == HeaderContentRange {
			partial = true
		}
	}
	if partial {
		log.Printf("[Warning] Partial Content! Cannot compress image with none-full data")
	} else {
		beforeCompressionLength := len(data)
		data = compress(data, content)
		afterCompressionLength := len(data)
		if afterCompressionLength != beforeCompressionLength {
			log.Printf("[Debug] Bytes: %d -> %d, ratio: %f", beforeCompressionLength, afterCompressionLength,
				float32(beforeCompressionLength) / float32(afterCompressionLength))
			response.Header.Set(HeaderContentLength, strconv.Itoa(afterCompressionLength))
		}
	}
	wr.WriteHeader(response.StatusCode)
	if _, err := wr.Write(data); err != nil {
		log.Printf("[Error] Failed to write resposne body: %s", err)
		return
	}
}